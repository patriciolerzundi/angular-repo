import { Component  } from '@angular/core';
import { Order } from '../models/order.model';
import { OrderRepository } from '../models/order.repository';


@Component({
    templateUrl: 'orderTable.component.html'
})

export class OrderTableComponent  {
    includeShipped = false;

    constructor(private repository: OrderRepository){}



    // Obtiene las orden de los productos
    getOrders(): Order[] {
        return this.repository.getOrders()
        .filter(o => this.includeShipped || !o.shipped);
    }

    // Marca el envio si esta listo o no
    markShipped(order: Order){
        order.shipped = true;
        this.repository.updateOrder(order);
    }

    //Elimina el envio
    delete(id: number) {
        this.repository.deleteOrder(id);
    }

}
import { Component } from '@angular/core';
import { ProductRepository } from '../models/product.repository';
import { Product } from '../models/product.model';

@Component({
templateUrl: 'productTable.component.html'
})

export class ProductTableComponent {

    constructor(private repository: ProductRepository){}

    // Obtengo los productos para listar en el componente
    getProducts(): Product[]{
        return this.repository.getProducts();
    }

    // Elimina un producto
    deleteProduct(id: number) {
        this.repository.deleteProduct(id);
    }

}

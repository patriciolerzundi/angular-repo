import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from './order.model';
//import { StaticDataSource } from './static.datasource';
import { RestDataSource } from './rest.datasource';


@Injectable()
export class OrderRepository {
    private orders: Order[] = [];
    private loaded: boolean = false;

    // se cambia de staticdatasource a restDatasource
    constructor(private dataSource: RestDataSource ) {}

    // load orders
    loadOrders() {
        this.loaded = true;
        this.dataSource.getOrders().subscribe(orders => this.orders = orders);
    }

    // Obtiene todas las orden
    getOrders(): Order[] {
        if (!this.loaded){
            this.loadOrders();
        }
        return this.orders;
    }

    // Guarda las orden
    saveOrder(order: Order): Observable<Order> {
        return this.dataSource.saveOrder(order);
    }
    
    // Update Orders
    updateOrder(order: Order) {
        this.dataSource.updateOrder(order).subscribe( order => {
            this.orders.splice(this.orders.findIndex(o => o.id == order.id),1, order);
        });
    }

    // eliminar Orden
    deleteOrder(id: number) {
        this.dataSource.deleteOrder(id).subscribe(order => {
            this.orders.splice(this.orders.findIndex(o => id == o.id));
        })
    }
    
    


}

import { Injectable } from '@angular/core';
import { Product } from './product.model';
//import { StaticDataSource } from './static.datasource';
import { RestDataSource } from './rest.datasource';
@Injectable()
export class ProductRepository {
    private products: Product[] = [];
    private categories: string[];

    // se cambia a staticdatasource a RestDataSource
    constructor(private dataSource: RestDataSource) {
        dataSource.getProducts().subscribe(data => {
            this.products = data;
            this.categories = data.map(p => p.category)
                // tslint:disable-next-line:triple-equals
                .filter((c, index, array) => array.indexOf(c) == index).sort();

        });
    }

    // Obtiene todo los productos
    getProducts(category: string = null): Product[] {
        return this.products
            // tslint:disable-next-line:triple-equals
            .filter(p => category == null || category == p.category);
    }

    // Se obtiene un producto con la id
    getProduct(id: number): Product {
        // tslint:disable-next-line:triple-equals
        return this.products.find(p => p.id == id);
    }

    // obtengo categorías
    getCategories(): string[] {
        return this.categories;
    }

    // guardar producto
    saveProduct(product: Product) {
        if (product.id == null || product.id == 0) {
            this.dataSource.saveProduct(product)
                .subscribe(p => this.products.push(p));
        } else {
            this.dataSource.updateProduct(product)
                .subscribe(p => {
                    this.products.splice(this.products.
                        findIndex(p => p.id == product.id), 1, product);
                });
        }
    }

    // elimina un producto
    deleteProduct(id: number) {
        this.dataSource.deleteProduct(id).subscribe( p => {
            this.products.splice(this.products.findIndex(p => p.id == id),1);
        })
    }




}

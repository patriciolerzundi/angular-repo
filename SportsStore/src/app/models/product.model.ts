export class Product {

    // cuando se colocan ? pueden ser opcionales u omitirlos
    constructor(
        public id?: number,
        public name?: string,
        public category?: string,
        public description?: string,
        public price?: number
    ) {}
}
